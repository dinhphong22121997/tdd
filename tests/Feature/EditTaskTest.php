<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EditTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_edit_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->put($this->getEditTaskRoute($task->id), $task->toArray());

        $response->assertStatus(302);
        $this->assertDatabaseHas('tasks', $task->toArray());
        $response->assertRedirect(route('tasks.index'));
    }
     /** @test */
    public function unauthenticated_user_can_not_edit_task() {
        $task = Task::factory()->create();
        $response = $this->put($this->getEditTaskRoute($task->id), $task->toArray());
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_not_edit_task_if_name_field_is_null() {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $data = [
            'name'=> null,
            'content' => $task->content
        ];
        $response = $this->put($this->getEditTaskRoute($task->id), $data);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_can_view_edit_task_form() {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get($this->getEditTaskViewRoute($task->id));
        $response->assertViewIs('tasks.edit');
    }

    /** @test */
    public function authenticated_user_can_see_name_required_text_if_validate_error_edit() {

        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();
        $task = Task::factory()->create();
        $response = $this->from($this->getEditTaskViewRoute($task->id))->put($this->getEditTaskRoute($task->id));
        $response->assertRedirect($this->getEditTaskViewRoute($task->id));
    }

    /** @test */
    public function authenticated_user_can_not_see_edit_task_form_view() {
        $task = Task::factory()->create();
        $response = $this->get($this->getEditTaskViewRoute($task->id));
        $response->assertRedirect('/login');
    }


    public function getEditTaskRoute($id) {
        return route('tasks.update', $id);
    }

    public function getEditTaskViewRoute($id) {

        return route('tasks.edit',$id);
    }
}
