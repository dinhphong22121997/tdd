<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    protected $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }
    public function index(Request $request)
    {
        $search = $request->input('search');
        $query = $this->task;

        if ($search) {
            $query = $query->where('id',  $search . '%')
                ->orWhere('name', 'LIKE', '%' . $search . '%');
        }

        $tasks = $query->latest('id')->paginate(6);

        return view('tasks.index', compact('tasks'));
    }

    public function store(CreateTaskRequest $request)
    {

        $this->task->create($request->all());
        return redirect()->route('tasks.index');
    }

    public function create()
    {

        return view('tasks.create');
    }

    public function edit($id)
    {
        $task = $this->task->find($id);
        return view('tasks.edit', compact('task'));
    }

    public function update(CreateTaskRequest $request, $id)
    {

        $this->task->find($id)->update($request->all());
        return redirect()->route('tasks.index');
    }
    public function destroy($id)
    {

        $this->task->find($id)->delete();
        return redirect()->route('tasks.index');
    }
}
