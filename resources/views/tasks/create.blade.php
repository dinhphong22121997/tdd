@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Create Task</h2>
                <form action="{{route('tasks.store')}}" method="post">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <input type="text" name="name" id="" class="form-group" placeholder="Name ...">
                            @error('name')<p class="text text-danger ">{{ $message }}</p> @enderror
                        </div>
                        <div class="card-body">
                            <input type="text" name="content" id="" class="form-group" placeholder="Content ...">
                        </div>
                    <button class="btn btn-success">Submit</button>
                    </div>

                </form>
                <a class="btn btn-primary" href="{{route('tasks.index')}}">Tro ve</a>
            </div>
        </div>
    </div>
@endsection
