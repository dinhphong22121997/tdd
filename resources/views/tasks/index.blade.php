@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <form action="" method="get">
                <div>
                    <input type="text" placeholder="Nhập ID hoac ten"  name="search">
                    <button type="submit">Tim</button>
                </div>
                <a class="btn btn-primary" href="{{route('tasks.create')}}">Them moi</a>
            </form>
            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Content</th>
                    <th>Actions</th>
                </tr>
                @foreach ($tasks as $task)
                    <tr>
                        <th>{{$task->id}}</th>
                        <th>{{$task->name}}</th>
                        <th>{{$task->content}}</th>
                        <th>
                            <form action="{{route('tasks.destroy', $task->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger">Delete</button>
                                <a href="{{route('tasks.edit', $task->id)}}" class="btn btn-warning">Edit</a>
                            </form>
                        </th>
                    </tr>

                @endforeach
            </table>
            {{$tasks->links()}}
        </div>
    </div>
@endsection


